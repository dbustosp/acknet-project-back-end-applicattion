/*
	File where are the functions related with user sessions
*/


// Simple, fast generation of RFC4122 UUIDS: http://en.wikipedia.org/wiki/Universally_unique_identifier
var uuid = require('node-uuid');
var User = require('../models/user');
var Session = require('../models/session');
var _ = require('underscore');




/*
	Return array after to parse the header of Authorization from client
 
  @param header String received from cliente for the header
  Authorization as for example: "Basic Asaskajajaasdkjads"
 
  @return Array where the first is the user and the second is the token

*/
var parseUserToken = function(header){
	console.log("parseUserToken: " + header);
	var base64 = _.first(header.split(" "));
	return (new Buffer(b64string, 'base64')).split(":");
};




/*

	Create new session. When it is created the callback is called.
	If already exists the session, it is eliminated and replaced by a new.

	@param usr User that create the new session
	@param callback
*/

exports.newSession = function(user, callback){
	// We search if already exists a a new session for the user
	console.log("****");
	console.log("user: " + user);
	console.log("****");

	createNewSession(user, callback);

	/*Session.findOne({user: user}, function(err, session){

		if(!session){
			console.log("newSession - !session");
			// There is not session for this user
			createNewSession(user, callback);
		}else{
			session.remove(function(err){
				if(err){
					console.log("remo - error");
					callback(err, null);
				}else{
					console.log("remo - !error");
					createNewSession(user, callback);
				}
			});
		}
	

	});*/
};


exports




/*
	Second part for creating new session. Finally it is created when the system make sure
	that the previous session was eliminated.

	@param user
	@param callback 
*/

var createNewSession = function(user, callback){

	var token = uuid.v4();

	Session.create({
		user: user,
		token: token
	}, function(err, session){
		if(err){
			callback(err, null);
		}else{
			callback(null, session);
		}
	});
};




/*
	Find user and session from a token and username.

	When finish it, call to the callback function

	@param String username
	@token Session token
	@callback A function. The first parameter is an error, the second and third the session.
*/
var findUserAndSession = function(user, token, callback){
	console.log("Buscando usuario");
	User.findOne({username: user }, function(err, usr){
		if(err){
			callback(err, null, null);
		}else{
			console.log("Usuario found!");
			if(!usr){
				callback(null, null, null);
			}else{
				console.log("Buscando session")
				Session.findOne({user: usr._id, token: token}, function(err, session){
					if(err){
						console.log("error en buscar sesion!")
						callback(err, null, null);
					}else{
						console.log("findUserAndSession - callback(null, user, session)");
						console.log("Mostrando session: " + session);
						callback(null, user, session);
					}
				});
			}
		}
	});
};




/*
	This function is used for authenticating users with bacic auth ine xpress
*/
exports.logged = function(user, token, callback){

	console.log("logged - Init");

	console.log("user: " + user);
	console.log("token: " + token);


	findUserAndSession(user, token, function(err, usr, session){
		console.log("usr: " + usr);
		console.log("token: " + session);

		if(session !== null && usr !== null){
			console.log("logged - callback(null, true);")
			callback(null, true);
		}else{
			console.log("logged - callback(null, false);")
			callback(null, false);
		}
	});
};


/*
Este middleware permite que los controladores puedan acceder a los usuarios en req.usr 
*/
exports.populateUserMiddleware = function(req, res, next){
  
  console.log(" Init - populateUserMiddleware");

  console.log("req.headers :  " + req.headers);

  console.log("req.headers.Authorization:  " + req.headers.Authorization);

  var userToken = parseUserToken(req.headers.Authorization);

  var usr = _.first(userToken);
  var token = _.last(userToken);

  console.log(" Init - populateUserMiddleware");

  findUserAndSesion(usr, token, function(err, user, sesion){
  	console.log(" Init - populateUserMiddleware");
    if(user !== null && sesion !== null)
    {
      req.usr = user;
    }
    next();
  });
};




