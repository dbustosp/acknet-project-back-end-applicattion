var mongoose = require('mongoose');

var NotificationSchema = new mongoose.Schema({
    message: {type: String, required: true, unique: true},
    date: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Notification', NotificationSchema);