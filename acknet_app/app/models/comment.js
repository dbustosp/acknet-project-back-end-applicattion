console.log("Cargando device");

var mongoose = require('mongoose');

var commentSchema = new mongoose.Schema({
	user: {type: mongoose.Schema.ObjectId, ref: 'User', required: true},
	story: {type: Number, ref: 'Story', required: true},
	body: {type: String},
	date: { type: Date,	default: Date.now },
	username: {type: String, required: true}
});

module.exports = mongoose.model('Comment', commentSchema);