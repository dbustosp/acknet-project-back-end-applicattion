var bcrypt   = require('bcrypt'), SALT_WORK_FACTOR = 10;
var validate = require('mongoose-validator').validate;
var mongoose = require('mongoose');
var validate = require('mongoose-validator').validate;

var userSchema = new mongoose.Schema({
    username: {type: String, unique: true, required: true},
    password: {type: String, unique: false, required: true},
    email: {
        type: String, 
        validate: [validate({message: "El email no es correcto"}, 'isEmail')],
        unique: true,
        require: true
    },
    messages: [{type: String}],
});


/*
    With this middleware we can create key hash
*/
userSchema.pre("save", function(next){
    
    var user = this;
    
    // only hash the password if it has been modified (or is new)
    if(!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt){
        if(err) return next();

        // hash the password along with our new salt
        bcrypt.hash(user.password, salt, function(err, hash){
            if(err) return next();

            // override the cleartext password with the hashed one
            user.password = hash;
            next();

        });
    });
});


/*
    With this method we can try if the keys are the same or not, if TRUE then we call to callback(null,true)
*/

userSchema.methods.comparePassword = function(password, callback){
    var user = this;

    bcrypt.compare(password, user.password, function(err, match){
        if(err) return callback(err);
        callback(null, match);
    });
}

module.exports = mongoose.model('User', userSchema);
