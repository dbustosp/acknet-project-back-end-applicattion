console.log("Cargando device");


var mongoose = require('mongoose');
var config = require('../../config');

var deviceSchema = new mongoose.Schema({
 	username: {type: String},
 	device: {type: String},
});

module.exports = mongoose.model('Device', deviceSchema);