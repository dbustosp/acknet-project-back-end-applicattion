console.log("Cargando postStory");

var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

var storySchema = new mongoose.Schema({

	user: {type: mongoose.Schema.ObjectId, ref: 'User', required: true},
	
	username: {type: String},
	
	body: {type: String},
	
	attachment: {
		type: {type: String},
		url: {type:String},
	},

	geolocation: {
		lat: {type: String},
		lon: {type: String},
		alt: {type: String},
	},

	date: { type: Date,	default: Date.now },

	comments: [{
		type: mongoose.Schema.ObjectId, 
		ref: 'User', 
		date: {type: Date, default: Date.now}, 
		body: {type: String},
	}], 

	likes: [{type: mongoose.Schema.ObjectId, ref: 'User'}],

});

storySchema.plugin(autoIncrement.plugin, 'Story');

module.exports = mongoose.model('Story', storySchema);
console.log("Expertado postSchema");