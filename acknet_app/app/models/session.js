/*
        Model for controlling the user sessions
*/
console.log("Cargando SessionSchema");
var mongoose = require('mongoose');
var User = require('../models/user');

var SessionSchema = new mongoose.Schema({

    user: {type: mongoose.Schema.ObjectId, ref: 'User', required: true},
    token: {type: String, unique: true, required: true},
    date: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Session', SessionSchema);