var vows = require('vows');
var assert = require('assert');
var User = require('../models/user');
var mongoose = require('mongoose');

var db = mongoose.connect('mongodb://localhost/acknet_test');
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));


//Context are executed in parallel, they are fully asynchronous
//Context: Is an object with an optional topic, zero o more vows and zero or more sub-contexts.
//Context usually contain topics and vows, which in combination define your test.

vows.describe('Create user').addBatch({

	'When system creates an user': {   								//Context
		topic: function(){
			User.create({
        		username: "dbustosp",
		        password: "1234",
		        email: "dbustosp@gmail.com"
      		}, this.callback);
  		},
		'Key should be hashed': function(err, usr){					// Vow		
			if(err) console.log('Can not create the user');			// Test the result of the topic
			assert.notEqual(usr.password, '1234');
		}
	},

	// This unit Test throw error because I have not implemented the hash yet
	'When system creates an user': {
		topic: function(){
			User.create({
        		username: "dbustos10",
		        password: "1234",
		        email: "dbustos10@gmail.com"
      		}, this.callback);
		},
		'The key should be validated':{
			topic: function(usr, err){
				if(err) console.log("Error to crate user");
				usr.comparePassword("1234", this.callback);
			},
			'Are the same?': function(err, match){
				assert.equal(match, true);
			}
		}
	}
}).export(module);
