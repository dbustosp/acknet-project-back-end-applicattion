var Device = require('../models/device');


exports.put_device = function(req, res){

	console.log("init -- put_device");

	console.log(req);

	var mobile = req.body.regid;
	var username = req.body.username;

	console.log("mobile: " + mobile );
	console.log("username: " + username );

	Device.create({device: mobile, username: username}, function(err, data){
		if(err){
			console.log("Error Device create: " + err);
			res.json({success:false, message:"Something wrong happen saving the picture.", error: err})
		}else{
			console.log("Exito create Picture: " + data);
			res.json({success:true, message:"Device save successfully."})
       	}
	});
};


exports.remove_device = function(req, res){
	console.log("init -- remove_device");

	var device = req.params.device;
	console.log("device a remover: " + device);

	Device.findOne({device:device}, function(err, dev){

		if(err){
			console.log("I am sorry, the system can not find your device id.");
			res.json({success:false, message:"I am sorry, the system can not find your device id."})
		}else{
			console.log("dev encontrado ya hora removiendo: " + dev);
			dev.remove(function(err){
				if(err){
					console.log("remove_device -- return false - not remove device");
					res.json({success: false, message: "Device Id could not finish"});
				}else{
					console.log("remove_device -- return true - LOGOUT");
					res.json({success: true, message:"Message removed successfully"});
				}
			});
		}
	});
};