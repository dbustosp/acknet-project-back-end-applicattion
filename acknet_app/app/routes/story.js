var fs = require('fs');

var Story = require('../models/story');
var User = require('../models/user');
var getYouTubeID = require('get-youtube-id');
var request = require('request');
var app = require('../../app');


// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');

AWS.config.loadFromPath('./aws.json');

var s3 = new AWS.S3({params: {Bucket: 'acknet', Key: 'myKey'}});

s3.createBucket(function() {
	s3.putObject({Body: 'Hello!'}, function() {
		console.log("Successfully uploaded data to myBucket/myKey");
	});
});


exports.create = function(req, res){

	console.log("--- init create story ---");
	
	var username = req.body.username;
	var body = req.body.body;
	var attachment = req.body.attachment;
	var geolocation = req.body.geolocation;
	var comments = null;
	var likes = null;

	var type_file = attachment.type;
	var source = attachment.source;
	var link = attachment.link;
	

	var lat = geolocation.lat;
	var lon = geolocation.lon;
	var alt = geolocation.alt;


	var KEY_ID;


	if(type_file === "image"){
		// The story is with image

		// Upload the image to Amazon
		// If all is good
			//take the link from amazon
			// update the variable link
			// Save in database
			// return TRUE with message
		// else
			// return null with the error

		var path_url = "http://s3.amazonaws.com/arcane-crag-4782.herokuapp.com/";
		var b64string = source;
		var id;

		
		User.findOne({username:username}, function(err, usr){

			if(err){
				res.json({success:false, message: "User not found."});
			}else{
				console.log("User found: " + usr._id);
				Story.find({user:usr._id}, function(err, data){
					if(err){
						console.log("ERROR !found: " + err);
						//Send message for error
						res.json({success:false, message: "Sorry, you are not in our database, you can't send posts."});
					}else{
						console.log("Story found.")
						var last_story = data.pop();
			    		if(last_story === undefined){
							console.log("last_picture null")
							id = 0;
						}else{
							console.log("last_picture NO null")
							id = last_story._id + 1;
							console.log(id);
						}

						var buf = new Buffer(b64string, 'base64');
						var extention = ".jpg";
						var key = username + "/" + id + extention;

						var s3bucket = new AWS.S3({params: {Bucket: 'arcane-crag-4782.herokuapp.com'}});				

						var params = {
							Key: key,
							Body: buf,
							ACL: 'public-read'
						};


						s3bucket.putObject(params, function(err, data) {
							if(err){
								console.log("Error putObject: " + err);
								res.json({success: false});
							}else{
								// TO Do Validate problemas with repeated images
						        var url = path_url + key;
						        console.log("url: " + url);
					        
						        var file_attachment = {
						        	type: type_file,
						        	url: url,
						        }

						        console.log("file_attachment: " + file_attachment);

						        var file_geolocation = {
						        	lat:"lat",
						        	lon:"lon",
						        	alt:"alt",
						        }

						        console.log("file_geolocation: " + file_geolocation);

						        console.log("usr: " + usr);

						        console.log("body: " + body);


						        // Add image to base de datos
						        Story.create({
						        	user:usr,
						        	username:username,
						        	body:body,
						        	attachment: file_attachment,
						        	geolocation: file_geolocation, 
						        }, function(err, story){
						        	if(err){
						        		console.log("Error Story create: " + err);
						        		res.json({success:false, message:"Something wrong happen saving the picture."})
						        	}else{
						        		console.log("Exito create Story: " + story);
						        		console.log("ENVIAAAAAANDOOOOOO");
						        		app.story_create(story);
						        		res.json({success:true, message:"Picture save successfully."})
						        	}
						       	});
								
							}
						});
					

					}
				

				});
			}


		});
	

	}else{

		if(type_file === "video"){
			console.log("revisando video link: " + link);
			
			var id = getYouTubeID(link);

			console.log("id video : " + id);

			if(id === null){
				id = "fakeId";
			}

			var url = "http://gdata.youtube.com/feeds/api/videos/"+id;
			
			request(url, function (error, response, body) {
				if (!error && response.statusCode == 200) {
					console.log(body) // Print the google web page.
					KEY_ID = id;
				}else{
					console.log("Sorry, the video does not exist in Youtube.");
					res.json({success:false, message:"Sorry, the video does not exist in Youtube."})
				}
			});

			var file_attachment = {
	    		type: type_file,
				url: id,
		    }


		}else {
			// Only text
	    	var file_attachment = {
	    		type: type_file,
				url: ""
		    }
		}

		// The story is with video
		User.findOne({username:username}, function(err, usr){
			if(err){
				res.json({success:false, message: "User not found."});
			}else{
		        // Add image to base de datos

		    	console.log("Agregando: " + file_attachment.type_file + " and " + file_attachment.url);

		        var file_geolocation = {
		        	lat:"lat",
		        	lon:"lon",
		        	alt:"alt",
		        }


		        console.log("username: ************ " + username);

		        Story.create({
		        	user:usr,
		        	username: username,
		        	body:body,
		        	attachment: file_attachment,
		        	geolocation: file_geolocation, 
		        
		        }, function(err, story){
		        	if(err){
		        		console.log("Error Story create: " + err);
		        		res.json({success:false, message:"Something wrong happen saving the picture."})
		        	}else{
		        		console.log("Exito create Story: " + story);
						console.log("ENVIAAAAAANDOOOOOO");
						app.story_create(story);
		        		res.json({success:true, message:"Picture save successfully."})
		        	}
		       	});
			}
		});
	}
};

exports.delete_story = function(req, res){
	var story = req.params.story;
	console.log("-- try delete story " + story);

	var header=req.headers['authorization']||'',        // get the header
    	token=header.split(/\s+/).pop()||'',            // and the encoded auth token
    	auth=new Buffer(token, 'base64').toString(),    // convert from base64
    	parts=auth.split(/:/),                          // split on colon
    	user=parts[0],
    	password=parts[1];

    console.log("Buscando la story: " + story);
	Story.findOne({_id:story}, function(err, str){
		if(err){
			console.log("The server can not find your story. Sorry.");
			res.json({success:false, message:"The server can not find your story. Sorry.", err:err});
		}else{
			if(!str){
				console.log("Sorry, the system did not find the story: " + str);
				res.json({success:false, message:"Sorry, the system did not find the story."});
			}else{
				if(user === str.username){
					str.remove(function(err){
						if(err){
							console.log("Sorry the system can not remove the story.");
							res.json({success:false, message: "Sorry the system can not remove the story.", err: err});
						}else{
							console.log("remove_story -- return true --");
							res.json({success: true, message:"Comment removed successfully"});
						}
					});
				}else{
					console.log("Sorry you can not remove this story.");
					res.json({success:false, message:"Sorry you can not remove this story."});
				}
			}
		}
	});

};

exports.render_timeline	 = function(req, res){
	console.log("render");

	Story.find({}, function(err, stories){
		if(err){
			res.json({success:false, message:"Sorry, something wrong happened fetching the stories."});
		}else{
			console.log("terminado");
			var stream = mu.compileAndRender('timeline.html', {stories: stories, nombre: "john"});
			stream.pipe(res);
		}
	});
};


exports.getStories = function(req, res){
	console.log("-- init getStories --");
	Story.find({}, function(err, stories){
		if(err){
			res.json({success:false, message:"Sorry, something wrong happened fetching the stories."});
		}else{
			console.log("terminado");
			res.json({success:true, stories:stories});
		}
	})
};