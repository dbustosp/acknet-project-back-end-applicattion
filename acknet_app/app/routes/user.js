var modelUser = require('../models/user');
var Device = require('../models/device');

var gcm = require('node-gcm');
var sender = new gcm.Sender('AIzaSyATxI4pDs9WCBnrMz8O-ia7_AQW8uF39IU');
var message = new gcm.Message();

var NEW_USER = "0";






/*
 * GET users listing.
 */

exports.getUsers = function(req, res){
	modelUser.find({}, function(err, users){
		if(err) res.send("Something wrong!");

		res.json({
			success: true,
			users: users
		}, 200);


	});
};


exports.index = function(req, res){
  res.send("respond with a resource");
};


exports.list = function(req, res){
	res.send("Usuarios!");
}


// Users routes

/*
	Create an user with the followings parameters en req.params:

	- username: Username user that user will enter when he will register.
	- password: Password user that user will enter when he will register.
	- email: Email user that user will enter when he will register.
*/
exports.create = function(req, res){
	var username = req.body.username;
	var password = req.body.password;
	var email = req.body.email;

	console.log("username: "+username);
	console.log("password: "+password);
	console.log("email: "+email);

	if(username === "" | password === "" || email === "" ){
		console.log("HEre");
		res.json({
			success: false,	
			message: "Something is wrong. The system need at least your username, password and email."
		});
	}else if(username === undefined | password === undefined || email === undefined ){
		console.log("Aca");
		res.json({
			success: false,
			message: "Something is wrong. The system requires all fields for registering you. "
		});
	}else{

		// Add databse
		console.log("Creando User");

		modelUser.create({
			username: username,
			password: password,
			email: email
		},
		function(err, usr){
			if(err){

				if(err.message === "Validation failed"){
					//Something is wrong with the validation.
					res.json({success: false, message: "The email intered is not an valid email."});
				}else if(err.name === "MongoError"){
					
					if(err.err.indexOf("email") != -1){
						res.json({success: false, message: "Sorry: Already exists an account with this email."});
					}else if(err.err.indexOf("username") != -1){
						res.json({success: false, message: "Sorry: Already exists an acoount with this username."});
					}else{
						res.json({success: false, message: err});
		        	}
				}else{
					res.json({success: false, message: err});
				}
			}else{

				// Send message to GCM that registered new USer
				

				message.addData("type", NEW_USER);
				message.addData("username", usr.username);
				message.delay_while_idle = 1;
				var registrationIds = [];

				Device.find(function(err, devices){
					


					if(err){
						// To do something
					}else{
						console.log("devices: " + devices);
						var attrName;
						var attrValue;

						for(var i=0;i<devices.length;i++){
							var device = devices[i];
							for(var key in device){
								attrName = key;
								if(attrName === "device"){
									attrValue = device[key];
									console.log("Pushiando: " + attrValue);
									registrationIds.push(attrValue);
								}
							}
						}

						sender.send(message, registrationIds, 4, function (err, result) {
							if(err){
								// To do something
							}else{
								console.log("result" + result);
							}
						});
					}
				});

				res.json({success: true, message: "User created successfully."}, 200);
			
			}
		});
	}
};



exports.get = function(req, res){
	var username = req.params.username;

	console.log(username);

	modelUser.findOne({username: username}, function(err, usr){
	
		if(err){
			res.json({success: false, message: "Ups! Something is wrong."});
		}else{

			if(!usr){
				res.json({success: false, message: "User did not find."});
			}else{
				n_user = {
					username: usr.username,
					email: usr.email
				};
				res.json({
					success: true,
					user: n_user,
				}, 200);
			}
		}
	});




}

exports.edit = function(req, res){

	var username = req.params.username;
	var password = req.body.password;
	var email = req.body.email;

	console.log("parametros: " + req.params + " " + req.body.password + " " + email);

	if(username === undefined){
		console.log("Username indefinido");
		res.json({success: false, message: "You must specify an user."});
	}else{
		modelUser.findOne({username:username}, function(err, usr){
			if(err){
				console.log("Ups! Something is wrong.");
				res.json({success: false, message: "Ups! Something is wrong."});
			}else{

				if(!usr){
					console.log("User not found.");
					res.json({success:false, message: "User not found."});
				}else{

					if(password !== undefined){
						console.log("Password is not undefined");
						usr.password = password;
					}else{
						console.log("Password is undefined");
					}


					if(email !== undefined){
						console.log("Email is not undefined");
						usr.email = email;
					}

					usr.save(function(err){

						if(err){
							console.log("Error to update user.");
							res.json({success: false, message: "Error to update user."});
						}else{
							console.log("OK");
							res.redirect('http://127.0.0.1:3000/user/' + username);
							//res.json({success: true, user: usr});
						}
					});
				}

			}


		});
	}


	
}