
var Comment = require('../models/comment');
var User = require('../models/user');
var Story = require('../models/story');

exports.get = function(req, res){
	var story = req.params.story;
	console.log("Get comments of story - id_story: " + story);

	Comment.find({story:story}, function(err, comments){

		if(err){
			console.log("I am sorry the system can not load the comments");
			res.json({success:false, message:"I am sorry the system can not load the comments", err:err});
		}else{
			console.log("Got comments successfully");
			res.json({success:true, message: "Got comments successfully", comments:comments});
		}
	});
};


exports.delete_comment = function(req, res){
	
	console.log("-- init delete comment --");

	//var story = req.params.story;
	var id_comment = req.params.comment;

	var header=req.headers['authorization']||'',        // get the header
    	token=header.split(/\s+/).pop()||'',            // and the encoded auth token
    	auth=new Buffer(token, 'base64').toString(),    // convert from base64
    	parts=auth.split(/:/),                          // split on colon
    	user=parts[0],
    	password=parts[1];

	Comment.findOne({_id:id_comment}, function(err, cmmt){
		if(err){
			console.log("Sorry, Error in the server.");
			res.json({success:false, message:"Sorry, Error in the server.", err:err});
		}else{
			if(!cmmt){
				console.log("Sorry, the comment does not exists.");
				res.json({success:false, message:"Sorry, the comment does not exists.", err:err});
			}else{
				console.log("Encontrado cmmt: " + cmmt);

				// If is not the owner of the comment
				if( cmmt.username === user){
					cmmt.remove(function(err){
						if(err){
							console.log("I'm sorry the system can not remove the comment.");
							res.json({success:false, message: "Sorry the system can not remove the comment.", err: err});
						}else{
							res.json({success: true, message:"Comment removed successfully"});
						}
					});
				}else{
					// Check if is the owner of the Story
					Story.findOne({_id:cmmt.story}, function(err, stry){
						if(err){
							console.log("Sorry, Error in the server.");
							res.json({success:false, message:"Sorry, Error in the server.", err:err});
						}else{
							if(!stry){
								console.log("Sorry, the story does not exists.");
								res.json({success:false, message:"Sorry, the comment does not exists.", err:err});
							}else{
								if( user === stry.username){
									cmmt.remove(function(err){
										if(err){
											console.log("Sorry the system can not remove the comment.");
											res.json({success:false, message: "Sorry the system can not remove the comment.", err: err});
										}else{
											console.log("remove_comment -- return true - LOGOUT username_removing: "+ user + "user dueno de la story: " + stry.username);
											res.json({success: true, message:"Comment removed successfully"});
										}
									});
								}else{
									res.json({success: false, message:"Sorry you are not authorized to remove this comment."});
								}
							}
						}
					});
				}
			}
		}
	});
};


exports.create = function(req, res){

	console.log("-- create_comment -- ");

	var username = req.body.username_posting;
	var body = req.body.comment;
	var story = req.body.key_story;

	console.log("Buscando story con id: " + story);

	Story.findOne({_id:story}, function(err, stry){

		if(err){
			console.log("Sorry, the story does not exists.");			
			res.json({success:false, message:"Sorry, the story does not exists.", err: err});
		}else{
			console.log("Buscando usuario que hace el comentario: " + username);
			User.findOne({username:username}, function(err, usr){
				if(err){
					console.log("I'm sorry, you are not in our database. You can not put comments.");
					res.json({success:false, message:"I'm sorry, you are not in our database. You can not put comments.", err:err});
				}else{
				
					console.log("Creando el comment: user " + usr + " ****** story: " + stry._id);
					Comment.create({user:usr, story: stry._id , body:body, username:username}, function(err, comment){					
						
						if(err){
							console.log("Something wrong happened to save the comment. " + err);
							res.json({success:false, message:"Something wrong happen saving the picture.", error: err})
						}else{
							console.log("Comment saved successfully!");
							res.json({success:true, message: "Comment saved successfully!"});
						}
					});
				
				}
			});
		}
	
	});
};


