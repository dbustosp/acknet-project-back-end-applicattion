var Notification = require('../models/notification');

exports.create = function(req, res){

	var message = req.body.message;

	Notification.create({message:message}, function(err, notification){
		if(err){
			res.json({success:false, message:"Notification did not save.", err:err});
		}else{
			res.json({success:true, message:"Notification saved", err:err});
		}
	});
};

exports.get = function(req, res){
	Notification.find({}, function(err, notifications){
		if(err){
			res.json({success:false, message:"Sorry, something wrong happened fetching the notifications.", err:err});
		}else{
			console.log("terminado");
			console.log(notifications);
			res.json({success:true, notifications:notifications});
		}
	})
};