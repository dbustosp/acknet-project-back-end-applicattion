var User = require('../models/user');
var Session = require('../models/session');
var sesion_f = require('../libs/session');


exports.validateToken = function(req, res){
	var token = req.body.token;
	if(token === undefined){
		console.log("token is undefined");
		res.json({success:false, message:"token is undefined"})
	}else{
		Session.findOne({token:token}, function(err, session){
			if(!session){
				res.json({success:false, message:"Session not found."})
			}else{
				res.json({success:true, message:"Session inicializated"})
			}
		});
	}
};


/*
	User login. The user is passed as parameter in the url.
	The key is send in the body request.

	URL: /session/:user + body {password: 'myPassword'}

	Return json file indicating sucess or failed

	Sucess:
	{
		success: true,
		token: token foe doing the authentication
	}

	failed:
	{
		success: false,
		message: Message indicating error
	}
*/

exports.login = function(req, res){
	
	//console.log(req);

	var user = req.body.username;
	var password = req.body.password;

	console.log("user:" + user);

	console.log("login - init");

	if(user === undefined || password === undefined){
		console.log("Missing parameters");
		res.json({success: false, message: "Missing parameters for completing the action"});
	}else{
		User.findOne({username: user}, function(err, usr){
			console.log("login findOne - init");
			if(!usr){
				console.log("User not found");
				res.json({success: false, message: "User or password no valid."})
			
			}else{

				console.log("password: " + password);

				usr.comparePassword(password, function(err, match){
					
					console.log("login  comparePassword - init");

					if(match){

						console.log("login  match - init");

						sesion_f.newSession(usr, function(err, session){	

							if(err){
								console.log("Server error new Session");
								console.log(err);
								res.json({success: false, token: "SOmething wrong to create new session"});
							}else{
								console.log("login - TRUE");
								res.json({success: true, token: session.token});
							}
						
						})
					}else{
						console.log("login - No match");
						res.json({success: false, message: "User and key no valids."});
					}
				});
			}
		});
	}
};

/*
	Finish session user. Require token.

	DELETE /session/:user + body {token: token}

	return the server:

		success:
			{
				success: true
			}

		failed:
			{
				success: false,
				message: Message indicating the error
			}
*/


exports.logout = function(req, res){

	console.log("logout - init ");

	var user = req.params.username;
	var token = req.params.token;

	if(token === undefined || user === undefined){
		res.json({success: false, message: "Missing parameters"});
		console.log("logout -- undefined");
	}else{
		User.findOne({username: user}, function(err, usr){
			if(err){
				console.log("logout -- err - findOne");
				res.json({success: false, message: "Something wrong happened in the server"});
			}else{
				if(!usr){
					console.log("logout -- User not found");
					res.json({succes:false, message: "User not found"});
				}else{
					Session.findOne({user: usr, token: token}, function(err, session){
						if(err){
							console.log("logout -- Session not found");
							res.json({success: false, message: "Something wrong happened in the server"});
						}else{
							if(!session){
								console.log("Session not valid: " + usr);
								res.json({success: false, message: "Session not valid"});
							}else{
								session.remove(function(err){
									if(err){
										console.log("logout -- return false - not remove session");
										res.json({success: false, message: "Session could not finish"});
									}else{
										console.log("logout -- return true - LOGOUT");
										res.json({success: true});
									}
								});
							}
						}
					});
				}
			}
		});
	}
};
