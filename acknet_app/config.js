

/*
 With the data settings, return the url which we can use to connect us to the mongoDB server
*/
var mode = "production";

module.exports.getDatabase = function(){
  if(mode === "production"){
    return 'mongodb://acknet_team:1234@ds047458.mongolab.com:47458/acknet';
  }else{
    return 'mongodb://localhost/acknet'
  }
};



/*
var develop = {
  mongodb: 'mongodb://localhost/acknet',
  port: 3000,
  cookie_secret: 'your secret here',
  getMongoURL: function(){
    return this.mongodb;
  }
};

var production = {
  mongodb: 'ds047458.mongolab.com:47458',
  mongoUser: 'acknet_team',
  mongoPassword: '1234',
  mongoCollection: 'acknet_test',
  port: 80,
  cookie_secret: "acknet_best_app",
  getMongoURL: getMongoURL

};
*/

/*

Get the setting:
 
@param env tell us if we are in production or development.
@return Server configuration

*/

/*
module.exports.getConfig = function(env){
  if('development' === env){
    console.log("Using develop config");
    return develop;
  }else{
    console.log("Using production config");
    return production;
  }
};
*/
