

/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./app/routes/index');
var user = require('./app/routes/user');

var path = require('path');
var config_f = require('./config');
var session = require('./app/routes/session');
var lib_session = require('./app/libs/session');
var user = require('./app/routes/user');
var device = require('./app/routes/device');
var story = require('./app/routes/story');
var comment = require('./app/routes/comment');
var notification = require('./app/routes/notification');


var fs = require("fs");

util = require('util');
mu = require('mu2');

mu.root = __dirname + '/app/views'

var app = express();
var http = require('http');
var server =  http.createServer(app);

var io = require('socket.io').listen(server, {log:false});


// Authenticator


var auth = express.basicAuth(lib_session.logged  );

//var auth = express.basicAuth(function(user, token, callback) {
//	console.log(user);
//	console.log(token);
//	callback(null /* error */, true);
//});

//Set develop mode we need export NODE_ENV=development
//var config = config_f.getConfig(app.get('env'));
console.log("This app is in mode: " + app.get('env'));

var mongoose = require('mongoose');

// Here we try connect to mongod hosted in http://mongolab.com
var db = mongoose.connect(config_f.getDatabase());
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

autoIncrement = require('mongoose-auto-increment');



// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/app/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());

app.use(express.limit('4mb'));


app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
//app.use(express.session());
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));



//Middleware for getting req.user
app.use(lib_session.populateUserMiddleware);

// bodyParser middleware
app.use(express.bodyParser());

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
  mu.clearCache();
}

app.get('/', routes.index);


// get all users
app.get('/users', user.getUsers);



/* 
	*************************
		Methods API REST
	*************************
*/

// Users: Create Get Edit Remove
// -----------------------------
/*
	Method: Post user
	Task: Create User
	Parameters required in the body: username, email, password
	Url: '/user/'
*/
app.post('/user', user.create);
app.get('/user/:username', user.get);
app.put('/user/:username', user.edit);


// Timeline: Create Get Edit Remove Story
// --------------------------------------

/*
	Method: Post story
	Task: Create story
	Parameters:
		var user = req.body.username;
		var body = req.body.bodyy;
		var 	attachment = req.body.attachment;
		var geolocation = req.body.geolocation;
		var comments = null;
		var likes = null;
	url: '/create'
*/
app.post('/story',  story.create);
app.get('/story', auth, story.getStories);
app.delete('/story/:story/:username', auth, story.delete_story);


// Device GCM: CreatE DELETE
// -------------------------
app.post('/registerDevice', device.put_device);
app.delete('/device/:device', device.remove_device)


// Comment: create delete
// -------------------------
/*
	Parameters: 
	req.body.username ==> People make the comment
	req.body.body ==> Content of the comment
	req.body.story ==> Story id which is posted
*/
app.post('/comment', auth, comment.create);

//URL: /comment/id_comment
app.delete('/comment/:comment',  auth, comment.delete_comment);

// URL: /coments/id_story
app.get('/comment/:story', auth, comment.get);



// Notification: create get
// --------------------------
app.post('/notification', notification.create);
app.get('/notification', notification.get);



// Session: validateToken
app.post('/validateToken', session.validateToken);



// Adress for sessions and login
app.post('/session', session.login);
app.delete('/session/:username/:token', session.logout);



app.get('/timeline', story.render_timeline);



io.sockets.on('connection', function (socket) {

  var socket_in = socket;
  socket_out = socket;
  
  console.log("coneccion establecida");
  
  //socket_out.on('my other event', function (data) {
  //  console.log("llego nuevo mesage");
  //  console.log(data);
  //});

});


exports.story_create = function(story){	
	console.log("exports send_emir");
	socket_out.emit('news', { type: "create_story", data: story });
};



//app.get('/send_emit', function(req, res){

//	send_emir();
//	res.send(200);
//});








server.listen(app.get('port'));





